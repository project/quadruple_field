/**
 * @file
 * Behavior for tabs formatter.
 */

(function ($) {

  'use strict';

  Drupal.behaviors.quadrupleFieldTabs = {
    attach: function () {

      $('.quadruple-field-tabs').tabs();

    }
  };

})(jQuery);
